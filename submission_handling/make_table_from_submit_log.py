#!/usr/bin/which python
import sys
import re

def get_type_of_task( line1, line2):
    return


def get_relevant_lines( logfiletext ):
    relevant_lines = []
    for line in logfiletext:
        new_sample = {}
        if not "Creat" in line:
            continue
        relevant_lines.append(line)
    return relevant_lines


def get_pairs_of_describing_lines( all_relevant_lines ):
    pairs_of_describing_lines = []

    for i in range(len(all_relevant_lines)):
        matchObj_1 = re.match( r'Creating (.*) production: (.*)', all_relevant_lines[i], re.M|re.I)
        if matchObj_1:
            matchObj_2 = re.match( r'Created transformation (.*)', all_relevant_lines[i+1], re.M|re.I)

        if matchObj_1 and matchObj_2:
            #print "matched: ", matchObj_1.group()
            print matchObj_1.group(),matchObj_2.group() 
            pairs_of_describing_lines.append(( matchObj_1.group(),matchObj_2.group()  ))
        
    return pairs_of_describing_lines

def create_dict_from_line_pairs(pairs_of_describing_lines):
    job_dict = {}
    # {"<name>" : {"name" : <name>, "prodID": <prodiID>, "type": <type>}, "<name2": trafoid, ...}
    for pair in pairs_of_describing_lines:
        name_line = pair[0].strip().split(" ")
        prodid_line = pair[1].strip().split(" ")
        job_name = name_line[-1]
        job_type = ""
        for prod_type in ["generation","simulation","overlay","reconstruction"]:
            if prod_type in name_line:
                job_type += prod_type
                #this gives "overlayreconstruction" for the overlay reconstruction productions, that is good!
        job_prodid = prodid_line[-1]

        job_dict[job_name] = {"name": job_name, "prodid": job_prodid, "type": job_type}

    return job_dict


    

def reduce_prod_name(prod_name):
    prod_name_list = prod_name.strip().split
    prod_name_list.remove("clic")
    prod_name_list.remove("gen")
    prod_name_list.remove("sim")
    prod_name_list.remove("CLIC_o3_v14")
    prod_name_list.remove("rec")
    prod_name_list.remove("overlay")

    prod_name = "_".join(prod_name_list)
    return prod_name

def check_if_same_sample(name1, name2):
    """check that the names of the GEN, SIM, REC steps really match,
    i.e. the first and last part (splitting with _) are the same and if there
    is a polarisation string like polm80, polp80, it should be identical"""
    #or make it so it *finds* in the first place the one which has the matching name? because I might not know yet which one it is. sometimes GEN,SIM,REC follow directly after each other, sometimes its n*GEN, n*SIM, n*REC.
    name1_red = reduce_prod_name(name1)
    name2_red = reduce_prod_name(name2)
    
    return name1_red==name2_red



def main(argv):

    submitlogfile = argv[1]
    
    submitlog = open(submitlogfile)

    relevant_text = get_relevant_lines ( submitlog )
    print relevant_text
    # list_of_GENs = []
    # list_of_SIMs = []
    # list_of_DSTs = []
    # for line in submitlog:
    #     new_sample = {}
    #     if not "Creat" in line:
    #         continue

    #     matchObj = re.match( r'Creating generation production: (.*)', line, re.M|re.I)


    #     if matchObj:
    #         print "matchObj.group() : ", matchObj.group()

    #         new_sample["GEN_name"] = matchObj.group(1).strip()

    #     print new_sample
    #     #new_sample["GEN"] = 
        # print line

    pairs = get_pairs_of_describing_lines(relevant_text)
    job_dict = create_dict_from_line_pairs(pairs)

    #make a list of the GEN names
    #reorder the dict by going through and finding the belonging SIM and REC:
#    { "<reduced name>" : {}"GEN_name", "GEN_prodid": ..., ...... same for sim and rec }}

    submitlog.close()
    
    return

if __name__ == "__main__":
    main(sys.argv)