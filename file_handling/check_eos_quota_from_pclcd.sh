ssh lxplus6 << EOF
export EOSHOME=/eos/user/s/schnooru/
env STAGE_HOST=castorpublic.cern.ch stager_qry -siH -S ilcdata | awk '{print " Castor Storage Free:  " \$6\$7}'
export EOS_MGM_URL="root://eospublic.cern.ch"
eos quota /eos/experiment/clicdp | grep zf | awk '{printf(" Eos Storage Free:  %6.2fT(%2.1f%)\n",(\$10 - \$4),((\$10 - \$4)/\$10*100))}' 
eos quota /eos/experiment/clicdp | grep zf | awk '{printf(" Eos    Files Free:    %6.2fM(%2.1f%)\n",(\$12 - \$6),((\$12 - \$6)/\$12*100))}'
EOF

