echo \;
echo "how many events per job? (usually 25 for 3TeV, 100 for 1.5TeV and 380GeV)
     "
read events_per_job
echo "Datatype? (DST or GEN)"
read data_type
echo assuming $events_per_job events per job;
echo "what are the prod ids? give a space separated list"
read list_of_prodids
echo \|prodID \|jobs done\| events done\|;
for i in $list_of_prodids;
do n_files=$(dirac-ilc-find-in-FC /ilc/prod/ ProdID=$i Datatype=$data_type | wc -l) ;
   are_files=$(dirac-ilc-find-in-FC /ilc/prod/ ProdID=$i Datatype=$data_type)
   if [ "$are_files" == "No files found" ]; then
       n_files=0
   fi
   echo \| $i \| $n_files \| "$(($n_files*$events_per_job))"\| ; 
done
