from ROOT import *
import sys
sys.path.append('/usr/lib64/python2.6/site-packages')
import numpy as np

def GetXYListFromFile(filename):
    xlist = []
    ylist = []
    for line in open(filename,"rb"):
        linesplit = line.split(" ")
        xlist.append(float(linesplit[0]))
        ylist.append(float(linesplit[2]))
        #errlist.append(float(linesplit[3].replace("\n","")))
    return xlist,ylist
# read the input and put it into arrays
ZHHlistx,ZHHlisty = GetXYListFromFile("hhz_0_0_xsec.txt")
HHvvlistx,HHvvlisty = GetXYListFromFile("hh_n1n1_0_0_xsec.txt")


gStyle.SetPadTickY(1)
gStyle.SetPadTickX(1)


ZHHarray_x  = np.array(ZHHlistx)
ZHHarray_y  = np.array(ZHHlisty)
HHvvarray_x = np.array(HHvvlistx)
HHvvarray_y = np.array(HHvvlisty)

c1 =  TCanvas("c1","c1",200,10,700,500)

c1.SetBottomMargin(0.18)
c1.SetLeftMargin(0.18)


gStyle.SetLineWidth(3)
gStyle.SetLabelSize(0.06)
gZHH= TGraph(len(ZHHarray_x), ZHHarray_x, ZHHarray_y)

gHHvv= TGraph( len(HHvvarray_x), HHvvarray_x, HHvvarray_y)

gHHvv.GetXaxis().SetTitle("#sqrt{s} [GeV]")

gHHvv.GetXaxis().SetTitleSize(0.07)
gHHvv.GetYaxis().SetLabelSize(0.06)
gHHvv.GetYaxis().SetTitle("#sigma [fb]")
gHHvv.GetYaxis().SetTitleSize(0.07)
gHHvv.GetXaxis().SetRangeUser(0,3000)



gHHvv.SetMarkerColor(kBlue)
gZHH.SetMarkerColor(kRed)
gHHvv.SetLineColor(kBlue)
gZHH.SetLineColor(kRed)

#tz = TText(0.5,0.5,"ZHH")
#tz.SetTextSize(0.5)
#tz.SetTextColor(kBlack)
#tz.SetTextFont(42)
#tz.DrawTextNDC(0.5,0.5,"ZHH" )
#

gHHvv.SetMarkerSize(2)
gZHH.SetMarkerSize(2)
#gHHvv.SetLineWidth(3)
#gZHH.SetLineWidth(3)

gHHvv.GetYaxis().SetRangeUser(0.00086349,1)

gHHvv.Draw("APL")
gZHH.Draw("PL")
gHHvv.SetTitle("")
c1.SetLogy()

#
#leg = TLegend(0.3,0.4,0.8,0.5)
#leg.AddEntry(gHHvv,"HH#nu #bar{#nu}","l")
#leg.SetBorderSize(0)
#
#leg.Draw()
tz = TLatex()
tz.SetTextSize(0.08)
tz.SetTextFont(42)
tz.SetTextColor(kBlack)
tz.DrawLatex(0.5,0.5,"ZHH")
tvbf = TLatex()
tvbf.SetTextSize(0.08)
tvbf.SetTextFont(42)
tvbf.SetTextColor(kBlack)
tvbf.DrawLatex(0.5,0.5,"HH#nu_{e}#bar{#nu}_{e}")

c1.Update()

raw_input()