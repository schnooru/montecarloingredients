# MonteCarloIngredients

Scripts and tools for Monte Carlo production and production management. Both for the physics (Whizard settings, validation codes) and for the production management (configuration, grid submission, grid data handling).

# Setup
For using WHIZARD, usually need to set up:
```
source /cvmfs/clicdp.cern.ch/software/WHIZARD/2.8.2/x86_64-slc6-gcc7-opt/setup.sh
```
or the corresponding relevant version.
