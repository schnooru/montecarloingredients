from ROOT import TCanvas, TH1D, TF1, TLorentzVector, TVector3, TFile, TLegend, gStyle

gStyle.SetOptStat(0)


canv= TCanvas("canv","canv",800,600)
gStyle.SetOptStat(0)

leg = TLegend(0.12,0.5,0.5,0.88 )
f_l4   = TFile.Open("histos_limit4_10k_jetsout_jets.root","r")
f_l16  = TFile.Open("histos_limit16_10k_jetsout_jets.root","r")
f_norh = TFile.Open("histos_noreshist_jetsout_jets.root","r")

h_l4 = f_l4.Get("histo_qq")
h_l4.SetLineWidth(3)
h_l4.SetLineColor(4)
h_l4.Draw("E1")
leg.AddEntry(h_l4,"limit 4","l")

h_l16 = f_l16.Get("histo_qq")
h_l16.SetLineWidth(3)
h_l16.SetLineColor(6)
h_l16.Draw("e1 same")
leg.AddEntry(h_l16,"limit 16","l")

h_norh = f_norh.Get("histo_qq")
h_norh.SetLineWidth(3)
h_norh.SetLineColor(7)
h_norh.Draw("e1 same")
leg.AddEntry(h_norh,"no resonance","l")

leg.Draw()
canv.Modified()
canv.Update()

raw_input()
f_l4.Close()
f_l16.Close()
f_norh.Close()
