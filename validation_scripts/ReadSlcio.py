#!/cvmfs/clicdp.cern.ch/DIRAC/Linux_x86_64_glibc-2.12/bin/python
from pyLCIO import IOIMPL
import sys
from ROOT import TCanvas, TH1D, TF1, TLorentzVector, TVector3, TFile


def MEQuarks( MCCollection, nquarks):
    #the first two particles in the collection that
    #- are not electron or positron or photon, but instead a quark
    #- have a parent that is electron or positron or photon(?)
    quarks = []
    for particle in MCCollection:
        if len(quarks) == nquarks:
            return quarks
        PDGID = particle.getPDG ()
        parents = particle.getParents()
        parentIsEpEm = all( abs(p.getPDG()) ==11 for p in parents )
        if abs(PDGID) <=6 and parentIsEpEm:
            quarks.append(particle)

    return "WARINING Did not find {} particles".format(nquarks)
    

def FinalStateParticles( MCCollection):
    #pdg>=100 and no daugthers
    allfinalstateparticles = []
    for particle in MCCollection:
        PDGID = particle.getPDG () 
        daughters = particle.getDaughters()
        parents = particle.getParents()
        if len(parents)>0 and len(daughters) == 0:
            allfinalstateparticles.append(particle)
            
    return allfinalstateparticles

def FinalStateJets( MCCollection):
    #make fourvectors of all the final state particles and then cluster them into jets
    jets = []

    #for p in MCCollection:
    #    if p.getGeneratorStatus() != 1:
    #        continue
            
    for particle in MCCollection  :
        jets.append(particle)
    
        
    return jets



def FillHistosFromEvents(prodid, reader ):
    histoname = "histo_{}".format(prodid)
    # create a histogram for the hit energies
    Histogram = TH1D( histoname, ';m_{jets} [GeV];Entries', 100, 0., 150. )

    nevent=0
    # loop over all events in the file
    for event in reader:
        nevent+=1
        if nevent%100 ==0:
            print("Processing event {}".format(nevent))
        # get the collection from the event
        #jetCollection = event.getCollection( "JetOut")
        truthparticleCollection = event.getCollection("MCParticle")

        #print truthparticleCollection.at(0).getDaughters().at(0).getPDG()
        #starting at index 0 , find the first daughter particle that is not an electron, positron or photon
        fsq_list = MEQuarks(truthparticleCollection, 2)
        #print len(fsq_list)
        mqq = fsq_list[0]

        q1, q2 = TLorentzVector(), TLorentzVector()
        q1.SetPxPyPzE(fsq_list[0].getMomentum()[0],fsq_list[0].getMomentum()[1],fsq_list[0].getMomentum()[2] , fsq_list[0].getEnergy()    )
        q2.SetPxPyPzE(fsq_list[1].getMomentum()[0],fsq_list[1].getMomentum()[1],fsq_list[1].getMomentum()[2] , fsq_list[1].getEnergy()    )
        mqq = (q1+q2).M()
        #Histogram.Fill(mqq)
        #fs_list = FinalStateParticles(truthparticleCollection)
        truthjetCollection = event.getCollection("Jet_VLC_R07_N2")
        fs_list = FinalStateJets(truthjetCollection)
        fs_all = TLorentzVector()
        for p in fs_list:
            new_p = TLorentzVector()
            new_p.SetPxPyPzE(p.getMomentum()[0],p.getMomentum()[1],p.getMomentum()[2] , p.getEnergy()    )
            fs_all+=new_p
        Histogram.Fill(fs_all.M())


        


        
#        for particle in truthparticleCollection:
#            PDGID = particle.getPDG ()
#            if abs(PDGID) <=6 and isPrimaryParticle:
#                quarks.append(particle)
#                
            
        #find the particles that I am interested in: all quarks I guess (before they decay...)
        #get all their invariant di-parton masses and put them in the histo => need to see some resonances
        #maybe I want all the particles that are daughters of Ws and Zs? but not all events have those I think


#        higgs1, higgs2, dihigss =  TLorentzVector(),  TLorentzVector(), TLorentzVector()
#        higgscandidatelist= []
#        higgses = []
#        #get the particles with pdgid 25 (higgs) which have 2 parents and one daughter
#
#        for particle in truthparticleCollection:
#            PDGID = particle.getPDG ()
#            if not PDGID == 25: continue
#            higgscandidatelist.append(particle)
#
#        #print("Found {} candidates for higgses".format(len(higgscandidatelist)))
#        for candidate in higgscandidatelist:
#            if len(candidate.getParents()) == 2 and len(candidate.getDaughters()) ==1:
#                higgses.append(candidate)
#
#
#        if len(higgses) !=2:
#            print("WARNING found not 2 but {} higgses".format(len(higgses)))
#
#    
#        higgs1.SetPxPyPzE(higgses[0].getMomentum()[0],higgses[0].getMomentum()[1],higgses[0].getMomentum()[2] , higgses[0].getEnergy()    )
#    
#    
#        higgs2.SetPxPyPzE(higgses[1].getMomentum()[0],higgses[1].getMomentum()[1],higgses[1].getMomentum()[2] , higgses[1].getEnergy()    )
#        MhhHistogram.Fill( (higgs1+higgs2).M())



    reader.close()


    
    return nevent , Histogram

def FileNumberFromName(filename):
    print filename
    return

def main(*args):

    canv= TCanvas("canv","canv",800,600)
    samplefile = "limit4_10k.slcio"
    samplefile = args[1]
    print("Using sample file {}".format(samplefile))
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open(samplefile)
    outfile = "histos_{}_jets.root".format(samplefile.replace(".slcio",""))
    hfile = TFile.Open(outfile,"RECREATE")
    nevents, histo = FillHistosFromEvents("qq", reader)
    histo.Draw()
    canv.Update()
    raw_input()
    canv.SaveAs( "m_jets_{}_jetclust.pdf".format(samplefile.replace(".slcio","") ))
    histo.Write()
    print("Writing histogram to {}".format(outfile))
    hfile.Close()
   
    return




if __name__ == "__main__":
    main(*sys.argv)
