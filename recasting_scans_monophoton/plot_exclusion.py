from ROOT import TGraph, TCanvas, kBlue, kRed, kDashed, TLegend, gStyle, gROOT, TFile
from array import array
import sys



#read in the table file, best in some kind of csv or numpy object because that would be the smoothest...
def xsec_dict_from_table_file(table_file):
    tfr = open(table_file)

    xsec_dict = {}
    for line in tfr:
        if line.startswith("#"):
            continue
        #print line
        mmed = line.split(" ")[0]
        mx = line.split(" ")[1]
        xsec = line.split(" ")[2]
        if not xsec_dict.get(mmed):
            xsec_dict[mmed] = {}
        xsec_dict[mmed][mx] = xsec

    return xsec_dict


#exclusion cross sections for X mass
def xsec_excl_380(mass_DM, sys_case):
    xsec_excl_dict = {
        100: {"nosys": 2.99302 ,"smallsys": 6.07793 ,"sys": 12.2477},
        110: {"nosys": 2.9639  ,"smallsys": 5.98718 ,"sys": 12.0337 },
        120: {"nosys": 2.93117 ,"smallsys": 5.88349 ,"sys": 11.7881},
        130: {"nosys": 2.88985 ,"smallsys": 5.75372 ,"sys": 11.4815},
        140: {"nosys": 2.82995 ,"smallsys": 5.5674  ,"sys": 11.0423 },
        150: {"nosys": 2.74718 ,"smallsys": 5.31777 ,"sys": 10.459 },
        160: {"nosys": 2.61451 ,"smallsys": 4.93389 ,"sys": 9.57264},
        170: {"nosys": 2.35377 ,"smallsys": 4.22294 ,"sys": 7.9613 },
        180: {"nosys": 1.73671 ,"smallsys": 2.74256 ,"sys": 4.75424}
    }

    if mass_DM < 100:
        return xsec_excl_dict[100][sys_case]
    for m in  range(100,190,10):
        #print mass_DM
        if mass_DM >= m and mass_DM < m+10:
            return xsec_excl_dict[m][sys_case]
    
def xsec_excl_3tev(mass_DM, sys_case):
    #if mass_DM < 200 or mass_DM > 1500: 
    #    print("Not intended for this mass range")
    if sys_case == "nosys":
        if mass_DM < 1450:
            return 2.4
        else:
            return 2.05
    if sys_case == "sys":
        if mass_DM < 1450:
            return 22.5
        else:
            return 18.72
    # -without systematic error the 95%CL upper limit is = 2.4 fb
    # -with systematic error on nunug of 0.001 and on eeg of 0.001 it is 9.5 fb
    # -with systematic error on nunug of 0.001 and on eeg of 0.001 it is 22.5 fb
    return 

def xsec_excl_nosys(mass_DM):
    return xsec_excl_380(mass_DM, "nosys")
    
def xsec_excl_sys(mass_DM):
    return xsec_excl_380(mass_DM, "sys")


def exclusion_arrays(table_file, excl_case="nosys", cme=380):
    if excl_case == "sys":
        print("generating the exclusions for the case of systematics considered on {}".format(table_file))
    else:
        print("generating the exclusions for the case of no systematics on {}".format(table_file))

    xsec_dict = xsec_dict_from_table_file(table_file)


    # the lists of the points on the exclusion line
    m_DM = array( 'd' )
    m_med = array( 'd' )

    #first get a sorted list of the mmeds:
    mmeds = []
    for m in xsec_dict.keys():
        mmeds.append(int(m))
    mmeds.sort()


    if cme == 380:
        mmed_check_max = 2500
        diff_max = 0.5
        mmed_turnover = 600
    if cme == 3000:
        mmed_check_max = 1000000
        diff_max = 5
        mmed_turnover = 4000

    text = "#M_mediator [GeV], m_excl_DM [GeV]\n"
    for mmed in mmeds:
        mmed = str(mmed)
        diff = 999999999999
        excl_mass = 0
        for mx in xsec_dict[mmed]:

            #print ("Current exclusion mass for mmed = {}: {}".format(mmed,excl_mass))
            xsec = float(xsec_dict[mmed][mx])
            if xsec == 0.0:
                continue #in that case do not use it!
            if cme==380:
                xsec_excl = xsec_excl_380(int(mx), excl_case)
            elif cme==3000:
                xsec_excl = xsec_excl_3tev(int(mx), excl_case)

            diff_limit = xsec - xsec_excl
            if abs(diff_limit) < diff and diff_limit > 0:
                diff = diff_limit
                excl_mass = mx
        if excl_mass > 0:
            print("m_med={}, sigma_exclusion={}, sigma(m_DM)={}, diff={}, m_DM_excl={}"
                  .format( mmed,xsec_excl, xsec_dict[mmed][excl_mass], diff, excl_mass))
        elif xsec > 0:
            print("m_med={}, sigma_exclusion={}, diff={}, m_DM_excl={}"
                  .format( mmed,xsec_excl,  diff, excl_mass))
        if int(mmed) > mmed_check_max and  diff > diff_max:
            print("For mmed={}, did not find a good exclusion.".format(mmed))
            continue

        #beyond (mmed_turnover) also exclude this point if the excl_mass is higher than the last two ones
        if int(mmed) > mmed_turnover:
            #print m_DM
            if (int(excl_mass) > m_DM[-1]) and (int(excl_mass) > m_DM[-2]):
                print "will exclude mass {} because it is larger than the two previous ones, {} and {}".format(excl_mass, m_DM[-1], m_DM[-2])
                continue
        text += "{} {}\n".format( mmed,  excl_mass) #, xsec_dict[mmed][excl_mass]
        m_DM.append(float(excl_mass))
        m_med.append(float(mmed))
        #m_med: xaxis, m_DM: yaxis
    return m_med, m_DM, text

def exclusion_graph( m_x, m_y, graphtitle="exclusion limits",graphname=""):
    g_excl = TGraph(len(m_x), m_x, m_y)
    g_excl.SetTitle(graphtitle)   



    return  g_excl

def save_graph(  graph_file, gr_text, graph ):
    rootfile = TFile.Open(graph_file + ".root","recreate")
    rootfile.cd()
    graph.Write()
    rootfile.Close()

    textfile = open(graph_file + ".txt", "w")
    textfile.write(gr_text)
    textfile.close()
    
    return
def plot_axial_vector(config_dict):
    gr_dict = {}

    #no systematics, axial
    excl_case = "nosys_axial"
    sys_case = excl_case.split("_")[0]
    table_file = config_dict[excl_case]["table_file"]
    m_med, mDM, gr_text = exclusion_arrays(table_file, sys_case, config_dict["cme"]) 
    gr_dict[excl_case] = exclusion_graph(m_med, mDM, config_dict["graphtitle"])
    graph_file = config_dict["couplings"] + "_" + excl_case + "_" + str(config_dict["cme"])
    save_graph( graph_file, gr_text, gr_dict[excl_case] )

    
    #no systematics, vector
    excl_case = "nosys_vector"
    sys_case = excl_case.split("_")[0]
    table_file = config_dict[excl_case]["table_file"]
    m_med, mDM, gr_text = exclusion_arrays(table_file, sys_case, config_dict["cme"]) 
    gr_dict[excl_case] = exclusion_graph(m_med, mDM, config_dict["graphtitle"])
    graph_file = config_dict["couplings"] + "_" + excl_case + "_" + str(config_dict["cme"])
    save_graph( graph_file, gr_text, gr_dict[excl_case] )

    #systematics, axial
    excl_case = "sys_axial"
    sys_case = excl_case.split("_")[0]
    table_file = config_dict[excl_case]["table_file"]
    m_med, mDM, gr_text = exclusion_arrays(table_file, sys_case, config_dict["cme"]) 
    gr_dict[excl_case] = exclusion_graph(m_med, mDM, config_dict["graphtitle"])
    graph_file = config_dict["couplings"] + "_" + excl_case + "_" + str(config_dict["cme"])
    save_graph( graph_file, gr_text, gr_dict[excl_case] )

    #systematics, vector
    excl_case = "sys_vector"
    sys_case = excl_case.split("_")[0]
    table_file = config_dict[excl_case]["table_file"]
    m_med, mDM, gr_text = exclusion_arrays(table_file, sys_case, config_dict["cme"]) 
    gr_dict[excl_case] = exclusion_graph(m_med, mDM, config_dict["graphtitle"])
    graph_file = config_dict["couplings"] + "_" + excl_case + "_" + str(config_dict["cme"])
    save_graph(  graph_file, gr_text, gr_dict[excl_case] )


    
    canv = TCanvas()
    canv.UseCurrentStyle()
    canv.SetBottomMargin(0.17)
    canv.SetLeftMargin(0.17)



    leg = TLegend(0.25,0.25,0.5,0.5)
    leg.SetBorderSize(0)
    #for gr in gr_dict:
    #    gr_dict[gr].Draw("same")
     


    gr_dict["nosys_vector"].Draw()
    gr_dict["nosys_vector"].SetLineColor(kBlue)
    leg.AddEntry( gr_dict["nosys_vector"], "vector, no syst.", "l" ) 

    gr_dict["nosys_vector"].GetXaxis().SetTitle("m_{med}[GeV]")
    gr_dict["nosys_vector"].GetXaxis().SetTitleOffset(1.2)
    gr_dict["nosys_vector"].GetXaxis().SetTitleSize(0.05)
    gr_dict["nosys_vector"].GetYaxis().SetTitle("m_{DM}[GeV]")
    gr_dict["nosys_vector"].GetYaxis().SetTitleOffset(1.2)
    gr_dict["nosys_vector"].GetYaxis().SetTitleSize(0.05)



    gr_dict["sys_vector"].Draw("same")
    gr_dict["sys_vector"].SetLineColor(kBlue)
    gr_dict["sys_vector"].SetLineStyle(kDashed)
    leg.AddEntry( gr_dict["sys_vector"], "vector, with syst." , "l" )

    gr_dict["nosys_axial"].Draw("same")
    gr_dict["nosys_axial"].SetLineColor(kRed)
    leg.AddEntry(gr_dict["nosys_axial"], "axial, no syst.", "l")

    gr_dict["sys_axial"].Draw("same")
    gr_dict["sys_axial"].SetLineColor(kRed)
    gr_dict["sys_axial"].SetLineStyle(kDashed)
    leg.AddEntry(gr_dict["sys_axial"], "axial, with syst.", "l")



    leg.Draw()

    canv.Modified()
    canv.Update()

   
    #g_excl.Draw()          
    #plotname= table_file.split("/")[-1].replace(".txt","")+"_exclusion.pdf"
    save_it = raw_input("Save it? [y]")
    if save_it == "y":
        plotname="exclusion_{}.pdf".format(config_dict["couplings"])
        canv.SaveAs(plotname)
    return

def plot_scalar(config_dict):
    gr_dict = {}

    #no systematics, scalar
    excl_case = "nosys_scalar"
    sys_case = excl_case.split("_")[0]
    table_file = config_dict[excl_case]["table_file"]
    m_med, mDM, gr_text = exclusion_arrays(table_file, sys_case, config_dict["cme"]) 
    gr_dict[excl_case] = exclusion_graph(m_med, mDM, config_dict["graphtitle"])
    graph_file = config_dict["couplings"] + "_" + excl_case + "_" + str(config_dict["cme"])
    save_graph( graph_file, gr_text, gr_dict[excl_case] )

    
  
    #systematics, scalar
    excl_case = "sys_scalar"
    sys_case = excl_case.split("_")[0]
    table_file = config_dict[excl_case]["table_file"]
    m_med, mDM, gr_text = exclusion_arrays(table_file, sys_case, config_dict["cme"]) 
    gr_dict[excl_case] = exclusion_graph(m_med, mDM, config_dict["graphtitle"])
    graph_file = config_dict["couplings"] + "_" + excl_case + "_" + str(config_dict["cme"])
    save_graph( graph_file, gr_text, gr_dict[excl_case] )

 

    
    canv = TCanvas()
    canv.UseCurrentStyle()
    canv.SetBottomMargin(0.17)
    canv.SetLeftMargin(0.17)



    leg = TLegend(0.25,0.25,0.5,0.5)
    leg.SetBorderSize(0)
    #for gr in gr_dict:
    #    gr_dict[gr].Draw("same")
     


    gr_dict["nosys_scalar"].Draw()
    gr_dict["nosys_scalar"].SetLineColor(kBlue)
    leg.AddEntry( gr_dict["nosys_scalar"], "scalar, no syst.", "l" ) 

    gr_dict["nosys_scalar"].GetXaxis().SetTitle("m_{med}[GeV]")
    gr_dict["nosys_scalar"].GetXaxis().SetTitleOffset(1.2)
    gr_dict["nosys_scalar"].GetXaxis().SetTitleSize(0.05)
    gr_dict["nosys_scalar"].GetYaxis().SetTitle("m_{DM}[GeV]")
    gr_dict["nosys_scalar"].GetYaxis().SetTitleOffset(1.2)
    gr_dict["nosys_scalar"].GetYaxis().SetTitleSize(0.05)



    gr_dict["sys_scalar"].Draw("same")
    gr_dict["sys_scalar"].SetLineColor(kBlue)
    gr_dict["sys_scalar"].SetLineStyle(kDashed)
    leg.AddEntry( gr_dict["sys_scalar"], "scalar, with syst." , "l" )



    leg.Draw()

    canv.Modified()
    canv.Update()

   
    #g_excl.Draw()          
    #plotname= table_file.split("/")[-1].replace(".txt","")+"_exclusion.pdf"
    save_it = raw_input("Save it? [y]")
    if save_it == "y":
        plotname="exclusion_{}.pdf".format(config_dict["couplings"])
        canv.SaveAs(plotname)
    return
    
def main(*args):
    #if not len(args)> 1:
    #    print("Please give the xsec table file as an argument, for example ../dm_with_whizard_production/mmed_vs_mx_vector.txt.")
    #table_file = args[1]
   
    gStyle.SetOptStat(0)
    gStyle.SetLineWidth(3)
    gStyle.SetTitleFont(42)
    gStyle.SetLabelFont(42)
    gStyle.SetPadTickX(1)
    gStyle.SetPadTickY(1)
    gStyle.SetLabelSize(0.04)
    gROOT.ForceStyle()


    config_dict_gDMglep1 = {
        "graphtitle" : "exclusion limits gDM=glep=1 for CLIC-380",
        "couplings"  : "gDM1_glep1",
        "cme" : 380,
        "nosys_axial" : {
            "table_file" : "../dm_with_whizard_production/mmed_vs_mx_axial.txt"
            },
        "nosys_vector" : {
            "table_file" : "../dm_with_whizard_production/mmed_vs_mx_vector.txt"
            },
        "sys_axial" : {
            "table_file" : "../dm_with_whizard_production/mmed_vs_mx_axial.txt"
            },
        "sys_vector" : {
            "table_file" : "../dm_with_whizard_production/mmed_vs_mx_vector.txt"
            }
    }
    config_dict_gDMglep2 = {
        "graphtitle" : "exclusion limits gDM=glep=2 for CLIC-380",
        "couplings"  : "gDM2_glep2",
        "cme" : 380,
        "nosys_axial" : {
            "table_file" : "../dm_with_whizard_production_glep_gDM_2/mmed_vs_mx_axial.txt"
            },
        "nosys_vector" : {
            "table_file" : "../dm_with_whizard_production_glep_gDM_2/mmed_vs_mx_vector.txt"
            },
        "sys_axial" : {
            "table_file" : "../dm_with_whizard_production_glep_gDM_2/mmed_vs_mx_axial.txt"
            },
        "sys_vector" : {
            "table_file" : "../dm_with_whizard_production_glep_gDM_2/mmed_vs_mx_vector.txt"
            }
    }
        
    config_dict_gDM1_glep025 = {
        "graphtitle" : "exclusion limits gDM=1, glep=0.25 for CLIC-380",
        "couplings"  : "gDM1_glep025",
        "cme" : 380,
        "nosys_axial" : {
            "table_file" : "../dm_with_whizard_production_glep0.25/mmed_vs_mx_axial.txt"
            },
        "nosys_vector" : {
            "table_file" : "../dm_with_whizard_production_glep0.25/mmed_vs_mx_vector.txt"
            },
        "sys_axial" : {
            "table_file" : "../dm_with_whizard_production_glep0.25/mmed_vs_mx_axial.txt"
            },
        "sys_vector" : {
            "table_file" : "../dm_with_whizard_production_glep0.25/mmed_vs_mx_vector.txt"
            }
    }

    config_dict_3tev_gDM1_glep01 = {
        "graphtitle" : "exclusion limits gDM=1, glep=0.1 for CLIC-3000",
        "couplings"  : "gDM1_glep01_3tev",
        "cme" : 3000,
        "nosys_axial" : {
            "table_file" : "../3TeV_dm_production/mmed_vs_mx_axial.txt"
            },
        "nosys_vector" : {
            "table_file" : "../3TeV_dm_production/mmed_vs_mx_vector.txt"
            },
        "sys_axial" : {
            "table_file" : "../3TeV_dm_production/mmed_vs_mx_axial.txt"
            },
        "sys_vector" : {
            "table_file" : "../3TeV_dm_production/mmed_vs_mx_vector.txt"
            }
    }
    config_dict_3tev_gDM1_glep1 = {
        "graphtitle" : "exclusion limits gDM=1, glep=1 for CLIC-3000",
        "couplings"  : "gDM1_glep1_3tev",
        "cme" : 3000,
        "nosys_axial" : {
            "table_file" : "../3TeV_dm_production_gDM1_glep1/mmed_vs_mx_axial.txt"
            },
        "nosys_vector" : {
            "table_file" : "../3TeV_dm_production_gDM1_glep1/mmed_vs_mx_vector.txt"
            },
        "sys_axial" : {
            "table_file" : "../3TeV_dm_production_gDM1_glep1/mmed_vs_mx_axial.txt"
            },
        "sys_vector" : {
            "table_file" : "../3TeV_dm_production_gDM1_glep1/mmed_vs_mx_vector.txt"
            }
    }


    config_dict_3tev_scalar_gDM1_glep1 = {
        "graphtitle" : "exclusion limits scalar DM for CLIC-3000",
        "couplings"  : "scalar_3tev",
        "cme" : 3000,
        "nosys_scalar" : {
            "table_file" : "/home/schnooru/CLIC/ESPPU/monophoton_DM/scalar_3TeV_glep1_gDM1/mmed_vs_mx_scalar.txt"
        },
        "sys_scalar" : {
            "table_file" : "/home/schnooru/CLIC/ESPPU/monophoton_DM/scalar_3TeV_glep1_gDM1/mmed_vs_mx_scalar.txt"
        },
    }
    config_dict_380gev_scalar_gDM1_glep1 = {
        "graphtitle" : "exclusion limits scalar DM for CLIC-380",
        "couplings"  : "scalar_380gev",
        "cme" : 380,
        "nosys_scalar" : {
            "table_file" : "/home/schnooru/CLIC/ESPPU/monophoton_DM/scalar_380GeV_glep1_gDM1/mmed_vs_mx_scalar.txt"
        },
        "sys_scalar" : {
            "table_file" : "/home/schnooru/CLIC/ESPPU/monophoton_DM/scalar_380GeV_glep1_gDM1/mmed_vs_mx_scalar.txt"
        },
    }

    config_dict = config_dict_gDMglep1
    #config_dict = config_dict_gDMglep2
    #config_dict = config_dict_gDM1_glep025
    config_dict = config_dict_3tev_gDM1_glep01
    #config_dict = config_dict_3tev_gDM1_glep1
    config_dict = config_dict_3tev_scalar_gDM1_glep1
    config_dict = config_dict_380gev_scalar_gDM1_glep1

    if not config_dict["couplings"].split("_")[0] == "scalar":
        plot_axial_vector(config_dict)

    else:
        plot_scalar(config_dict)


if __name__ == "__main__":
    main(*sys.argv)

