import glob

def PropertiesOfProcessname(procname):
    prop_dict = {}
    prop_dict["mmed"] = procname.split("_")[2]
    prop_dict["mx"] = procname.split("_")[4]
    # axial or vector
    prop_dict["case"] = procname.split("_")[5] 
    return prop_dict

def GetXSecTable(table_file,procnames, case="axial"):
    tablefile = open(table_file,"w")
    tablefile.write("#mmed [GeV], mx [GeV] xsec [fb], xsec error [fb]\n")
    for procname in procnames:
        tableline = ""
        #get the cross section and error out of the log file
        logfile = open('{}/whizard.log'.format(procname))
        xsec,xsecerr = "0", "0"

        for line in logfile:
            if line.startswith("xdpairs:"):
                xsec_all = next(logfile)
                xsec = xsec_all.split()[0]
                xsecerr = xsec_all.split()[2]
        if PropertiesOfProcessname(procname)["case"]!=case:
            continue
        mmed = PropertiesOfProcessname(procname)["mmed"]
        mx =  PropertiesOfProcessname(procname)["mx"]


        tableline += "{} {} {} {}\n".format(mmed,mx,xsec, xsecerr)
        tablefile.write(tableline)
    tablefile.close()
    return  

def main():


    procnames = glob.glob("xd_mmed_*")
    #print(procnames)
    table_file = "mmed_vs_mx_axial.txt"
    GetXSecTable(table_file,procnames,"axial")
    table_file = "mmed_vs_mx_vector.txt"
    GetXSecTable(table_file,procnames,"vector")

if __name__ == "__main__":
    main()
