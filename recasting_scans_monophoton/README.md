In order to do the recasting of the limits, the following steps are needed:

+ run Whizard for a large range of parameter combinations, by doing this in bash:

```
function runwhizard {
mmed=$1
mx=$2
mkdir xd_mmed_${mmed}_mX_${mx}_axial
cp xd_axial.sin xd_mmed_${mmed}_mX_${mx}_axial
mkdir xd_mmed_${mmed}_mX_${mx}_vector
cp xd_vector.sin xd_mmed_${mmed}_mX_${mx}_vector
sed -i "s/MY1 = 1000/MY1 = ${mmed}/g" xd_mmed_${mmed}_mX_${mx}_vector/xd_vector.sin
sed -i "s/MY1 = 1000/MY1 = ${mmed}/g" xd_mmed_${mmed}_mX_${mx}_axial/xd_axial.sin
sed -i "s/MXd = 100/MXd = ${mx}/g" xd_mmed_${mmed}_mX_${mx}_vector/xd_vector.sin
sed -i "s/MXd = 100/MXd = ${mx}/g" xd_mmed_${mmed}_mX_${mx}_axial/xd_axial.sin
cd xd_mmed_${mmed}_mX_${mx}_axial
whizard xd_axial.sin
cd ../
cd xd_mmed_${mmed}_mX_${mx}_vector
whizard xd_vector.sin
cd ../
}
```

and then run it over the parameters:
```
for i in 100 120 140 160 180 ; do cd xd_${i}_axial/ ; whizard xd_axial.sin; cd ../ ; done
```

The range is manually chosen based on  physics considerations

+ obtain a cross section table

```
python get_xsec_table.py
```
in the directory in which the scan was started. This results in a file <table.txt>.


+ plot the exclusion based on the cross section table. For this, the limits obtained in the original study are implemented into the script (currently hardcoded as they never changed before, might need to update)
```
python plot_exclusion.py <table.txt>
```