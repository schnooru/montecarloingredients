######################################
# prepare sindarin files
######################################
import os, subprocess


def CreateSindarinForProcess(polarisation, kappa):
    originaldir = os.getcwd()
    #cd in the right proc directory
    os.chdir(originaldir + "/"+ polarisation)
    os.system('pwd')

    WriteSindarinFileProcCME(polarisation,kappa)
    os.chdir(originaldir)
    return

    
def WriteSindarinFileProcCME(polarisation,kappa):
    text = ""
    text += "model = SM_dim6\n"
    text += "fac_gh3 = {}\n".format(kappa)
    
    text += "mtop   = 174 GeV\n"  
    text += "wtop   = 1.37 GeV   \n"
    text += "mW     = 80.45 GeV  \n"
    text += "wW     = 2.071 GeV  \n"
    text += "mZ     = 91.188 GeV \n"
    text += "wZ     = 2.478 GeV  \n"
    text += "mH     = 125 GeV    \n"
    text += "wH     = 0.00407 GeV\n"


    text += "mH = 126 GeV\n"
    text += "process decay_proc = e1, E1 => H, H , nue, nuebar\n"
    text += "compile\n"
    text += "sqrts = 3000 GeV\n"


    text += "beams = e1, E1  => circe2 => isr, isr\n"
    text += "$circe2_file = \"/cvmfs/clicdp.cern.ch/software/WHIZARD/circe_files/CLIC/3TeVeeMapPB0.67E0.0Mi0.15.circe\"\n"
    text += "$circe2_design = \"CLIC\"\n"
    text += "?circe2_polarized = false\n"
    text += "?keep_beams        = true\n"
    text += "!isr_order         = 3\n"
    text += "?isr_handler       = true\n"
    text += "isr_alpha          = 0.0072993\n"
    text += "isr_mass           = 0.000511\n"
    if polarisation == "negpol":
        text += "beams_pol_density  = @(-1), @()\n"
        text += "beams_pol_fraction = 80%, 0%\n"
    if polarisation == "pospol":
        text += "beams_pol_density  = @(+1), @()\n"
        text += "beams_pol_fraction = 80%, 0%\n"
        
        
    text += "integrate (decay_proc) {iterations = 10:100000:\"gw\", 5:200000:\"\"}\n"
    text += "show(results)\n"

    sindarinfile = open("{}_{}.sin".format(polarisation,kappa),"w")
    sindarinfile.write(text)
    sindarinfile.close()
    return

def GetXSecTable(polarisation,kappas):
    tablefile = open("{}.txt".format(polarisation),"w")
    #tablefile.write("CME [GeV], xsec [fb], xsec error [fb]\n")
    for kappa in kappas:
    #for cme in xrange(100,510,10):
        tableline = ""
        tableline += "{}  ".format(kappa)
        #get the cross section and error out of the log file
        logfile = '{}_{}.log'.format(polarisation,kappa)
        xsec_all = subprocess.Popen(['grep','+-','{}/{}'.format(polarisation,logfile)], stdout=subprocess.PIPE)
        xsec_long = xsec_all.communicate()[0]
        if xsec_long.rfind("+-") <0:
            #something went wrong, no +- is found!
            print("Did not get a correct cross section for {} and {}".format(polarisation,kappa))
            continue
        xsec    = xsec_long.split("+-")[0]
        xserror = xsec_long.split("+-")[1].replace("fb","")
        tableline += "{} {}".format(xsec,xserror)
        tablefile.write(tableline)

    tablefile.close()
    return


def runWhizard(polarisation, kappa ):
    originaldir = os.getcwd()
    os.chdir(originaldir + "/"+ polarisation)
    os.system('pwd')

    subprocess.call(['whizard','-r','{}_{}.sin'.format(polarisation,kappa),'-L{}_{}.log'.format(polarisation,kappa)])
    os.chdir(originaldir)
    return
    
def main():

    #need python 2.7: setupILCSOFT
    #need whizard of course: source /cvmfs/clicdp.cern.ch/software/WHIZARD/2.6.3/x86_64-slc6-gcc7-opt/setup.sh


    kappas = ["{0:.1f}".format(i*0.1) for i in range(1,26,1)]


    polarisation = "negpol"
    #polarisation = "pospol"
    #polarisation = "unpolarised"
    
    for k in kappas:
        print k
        #CreateSindarinForProcess(polarisation, k)
        #runWhizard(polarisation, k)

    GetXSecTable(polarisation, kappas)

    return

if __name__ == "__main__":
    main()
