######################################
# prepare sindarin files for running a
# scan of different center-of-mass
# energies to obtain the cross-section
# dependence
# ulrike.schnoor@cern.ch
######################################
import os, subprocess


def CreateSindarinForProcess(procname):
    originaldir = os.getcwd()
    #cd in the right proc directory
    os.chdir(originaldir + "/"+ procname)
    os.system('pwd')

    #loop through the cmes
    for cme in xrange(100, 10100, 100):

        #create there a sindarin file with the name proc_cme
        WriteSindarinFileProcCME(procname,Procstring(procname), cme)
    os.chdir(originaldir)
    return
def Procstring(procname):
    if procname == "ggWW": return "\"W+\",\"W-\""
    if procname == "ggWWWW": return "\"W+\",\"W-\",\"W+\",\"W-\""
    if procname == "ggWWZ": return "\"W+\",\"W-\",Z"
    if procname == "ggWWZZ": return "\"W+\",\"W-\",Z,Z"
    if procname == "ggZZ": return "Z,Z"
    if procname == "ggtt":
        print procname
        return "t, tbar "
    if procname == "ggbb": return "b, bbar "
    if procname == "ggcc": return "c, cbar "
    if procname == "ggee":
        return "e1, E1 "
    print "Did not manage to find out what process you mean"
    return
    
def WriteSindarinFileProcCME(procname,procstring, cme):
    print procstring
    text =""
    text+="######################################################\n"
    text+="# gamma gamma collisions\n"
    text+="# ulrike.schnoor@cern.ch\n"
    text+="######################################################\n"
    text+="! Model and Process block\n"
    text+="model        = SM\n"
    text+="process ggproc = A, A => {}\n".format(procstring)
    text+="compile\n"
    text+="! Beam block\n"
    text+="sqrts   = {} GeV\n".format(cme)
    text+="beams   = A, A\n"
    text+="integrate (ggproc)\n"
    text+="show(results)\n"
    sindarinfile = open("{}_{}.sin".format(procname,cme),"w")
    sindarinfile.write(text)
    sindarinfile.close()
    return

def GetXSecTable(procname):
    tablefile = open("{}.txt".format(procname),"w")
    #tablefile.write("CME [GeV], xsec [fb], xsec error [fb]\n")
    for cme in xrange(100, 10100, 100):
        tableline = ""
        tableline += "{}  ".format(cme)
        #get the cross section and error out of the log file
        logfile = '{}_{}.log'.format(procname,cme)
        xsec_all = subprocess.Popen(['grep','+-','{}/{}'.format(procname,logfile)], stdout=subprocess.PIPE)
        xsec_long = xsec_all.communicate()[0]
        if xsec_long.rfind("+-") <0:
            #something went wrong, no +- is found!
            print("Did not get a correct cross section for {} and {}".format(procname,cme))
            continue
        xsec    = xsec_long.split("+-")[0]
        xserror = xsec_long.split("+-")[1].replace("fb","")
        tableline += "{} {}".format(xsec,xserror)
        tablefile.write(tableline)

    tablefile.close()
    return


def runWhizard(procname):
    originaldir = os.getcwd()
    os.chdir(originaldir + "/"+ procname)
    os.system('pwd')
    for cme in xrange(100, 10100, 100):
        subprocess.call(['whizard','-r','{}_{}.sin'.format(procname,cme),'-L{}_{}.log'.format(procname,cme)])
    os.chdir(originaldir)
    return
    
def main():

    #need python 2.7: setupILCSOFT
    #need whizard of course: source /cvmfs/clicdp.cern.ch/software/WHIZARD/2.6.3/x86_64-slc6-gcc7-opt/setup.sh

    procnames = ["ggWW", "ggWWWW", "ggWWZ","ggWWZZ","ggZZ"]
    procnames = ["ggWW", "ggWWWW", "ggWWZ","ggWWZZ"]
    procnames = ["ggtt","ggbb","ggcc","ggee"]
    
    #runWhizard("ggtt")
    #etc

    for p in procnames:
        print p
        CreateSindarinForProcess(p)
        #GetXSecTable(p)

    

    return

if __name__ == "__main__":
    main()
